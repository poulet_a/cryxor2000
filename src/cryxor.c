/*
** cryxor.c for  in /home/poulet_a/projets/cryxor2000
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Fri Feb 14 12:15:46 2014 poulet_a
** Last update Fri Feb 14 12:34:34 2014 poulet_a
*/

#include <stdlib.h>
#include "my.h"

/*
** 62 chars
*/
#define BASE_LEN 62
#define BASE "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN1234567890"

void    cryxor_put_usage()
{
  my_putstr("Usage : cryxor200 <message> <key>\n");
  my_putstr("Encrypt a message with a key and output the result.\n");
}

char	cryxor_encrypt_one(char m, char k)
{
  int	pm;
  int	pk;

  pm = my_isin(m, BASE);
  pk = my_isin(k, BASE);
  if (pm == -1)
    return (' ');
  else if (pk == -1)
    return (m);
  else
    return (BASE[(pm ^ pk) % BASE_LEN]);
}

int		main(int ac, char **av)
{
  int	lm;
  int	lk;
  int	i;
  char	*out;

  if (ac != 3)
    {
      my_putstr("Error : ");
      cryxor_put_usage();
      return (-1);
    }
  RET_LONE_LONE((lm = my_strlen(av[1])));
  RET_LONE_LONE((lk = my_strlen(av[2])));
  RET_NULL_LONE((out = malloc(lm)));
  while (i < lm)
    {
      out[i] = cryxor_encrypt_one(av[1][i], av[2][i % lk]);
      i++;
    }
  my_putstr(out);
  return (0);
}
